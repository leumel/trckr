from scrapy import Spider

class BitTicker(Spider):
    name = "bit"
    start_urls = ["https://charts.bitcoin.com/"]

    def parse(self, response):
        print(response.css("body div.container div.billboard div.text-center a.chart-link p#bb-price::text").extract())