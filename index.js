const dotenv = require("dotenv")
dotenv.load()

const route_fb = require("./routes/fb.js")

const utils = require("./sources/utils.js")
const service = require("./sources/service.js")

const express = require("express")
const schedule = require('node-schedule')
const bodyparser = require('body-parser')
const io = require("socket.io")
const ioclient = require("socket.io-client")
const path = require("path")
const app = express()

const PORT = process.env.PORT

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:false}))

app.use("/fb", route_fb)

app.listen(PORT, function () {
    console.log(`listening to PORT ${PORT}`)
})

schedule.scheduleJob("*/30 * * * * *", function () {
    utils.getConfig(path.resolve(__dirname),function(config){
        service.getCurrentPrice(config.SUPPORTED_DCS, config.SUPPORTED_FIATS)
    })

    const { exec } = require('child_process');
    exec('python test.py', (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            return;
        }
        
        
    });
})

var socket = ioclient.connect("wss://streamer.cryptocompare.com")

socket.once('connected', function(){
    "im connected"
})