var fs = require("fs")
var path = require("path")

function Utils(){

}

Utils.getConfig = function(parentDirPath, callback){
    fs.readFile(path.join(parentDirPath,".config"),function(err, file){
        callback(JSON.parse(file))
    })
}

module.exports = Utils