const EP_PRICE = "https://min-api.cryptocompare.com/data/price"
const constants = require("./constants.js")
const fs = require("fs")
const path = require("path")

var superagent = require("superagent")

function Service(){

}

Service.getCurrentPrice = function(coins, fiats){
    superagent.get(EP_PRICE)
        .query({fsym: coins.toString(), tsyms:fiats.toString()})
        .end(function(err, res){
            console.log(res.body)
        })
}

module.exports = Service

