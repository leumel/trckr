var router = require("express").Router()

router.use(function(req,res,next){
    next()
})

router.get("/",function(req,res){
    console.log(req.query)
    if(req.query["hub.verify_token"] == process.env.FB_VERIFY_TOKEN){
        res.send(req.query["hub.challenge"])
        return
    }
    res.status(403).send("Failed")
})

router.post("/",function(req,res){
    console.log(req.body)
    console.log(req.body.entry[0].messaging)
    res.send("success")
})

module.exports = router